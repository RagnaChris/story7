from django.test import TestCase, Client
from django.urls import resolve
from .views import *

# Create your tests here.
class Story7Test(TestCase):
	def test_ganti_is_exist(self):
		response = Client().get('/ganti/')
		self.assertEqual(response.status_code, 200)

	def test_ganti_using_ganti_template(self):
		response = Client().get('/ganti/')
		self.assertTemplateUsed(response, 'ganti.html')

	def test_ganti_using_ganti_func(self):
		found = resolve('/ganti/')
		self.assertEqual(found.func, ganti)