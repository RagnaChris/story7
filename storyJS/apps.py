from django.apps import AppConfig


class StoryjsConfig(AppConfig):
    name = 'storyJS'
